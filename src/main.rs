use std::collections::HashMap;

fn brute_force(n: u64) -> u64 {
    let mut best: u64 = n;
    let mut sub_prob_sol: u64;
    for candidate in 1..(n - 1) {
        sub_prob_sol = candidate * brute_force(n - candidate);
        if sub_prob_sol > best {
            best = sub_prob_sol;
        }
    }
    best
}

fn solve(n: u64) -> u64 {
    // A slightly less dumb brute force
    let mut cache: HashMap<u64, u64> = HashMap::new();
    solve_with_cache(n, &mut cache)
}

fn solve_with_cache(n: u64, cache: &mut HashMap<u64, u64>) -> u64 {
    // Sum must = n
    // Product of elements must be maximized
    let mut best: u64 = n;
    let mut required: u64;
    let mut sub_prob_sol: u64;
    // The [n/2, n] candidates are the same as the first half but reversed
    for candidate in 2..(n / 2 + 1) {
        required = n - candidate;
        if let Some(cached) = cache.get(&required) {
            // We already found that value
            sub_prob_sol = cached.clone();
        } else {
            sub_prob_sol = solve_with_cache(required, cache);
        }
        sub_prob_sol = candidate * sub_prob_sol;
        if sub_prob_sol > best {
            best = sub_prob_sol;
        }
    }
    cache.insert(n, best);
    best
}

fn main() {
    let n = 15;
    println!("{}", solve(n));
    println!("{}", brute_force(n));
}

#[cfg(test)]
mod tests {
    use super::{brute_force, solve};
    use std::time::Instant;

    #[test]
    fn works() {
        assert_eq!(solve(1), 1);
        assert_eq!(solve(2), 2);
        assert_eq!(solve(4), 4);
        // 2 * 3
        assert_eq!(solve(5), 6);
        // (2 * 2) * 3
        assert_eq!(solve(7), 12);
        // 2 * 3 * 3
        assert_eq!(solve(8), 18);
        // 2 * 3 * 3 * 3
        assert_eq!(solve(11), 54);
        assert_eq!(solve(16), brute_force(16));
    }

    #[test]
    fn speed() {
        let now = Instant::now();
        solve(18);
        let time_solve = now.elapsed();
        brute_force(18);
        let time_brute = now.elapsed();
        // Must be 10 times faster
        assert!(time_solve.as_secs_f64() < time_brute.as_secs_f64() / 10.0);
    }
}
